#include <stdio.h>
#include <stdlib.h>
#define RANDO (rand() % 3)
#define TRIES 1000

// Kiest het cijfer tussen 0 en 2 dat niet gelijk is aan de argumenten
int TheOtherThing(int first, int second){
        int i;
        for(i=0; i == first || i == second; i++)
                ;
        return(i);
}

int main(){
        int score = 0;
        int choice, solution, open;
        for(int i=1;i<TRIES;i++){
                choice = RANDO;
                //je kiest een willekeurige deur
                solution = RANDO;
                //je weet niet waar de oplossing zit
                open = TheOtherThing(choice,solution);
                //een lege deur wordt geopend
                choice = TheOtherThing(choice,open);
                //je wisselt van deur
                if(choice == solution)
                        score++;
                        //proficiat met je lambo
        }
        printf("Correct in %d out of %d tries\n", score, TRIES);
        return(0);
}
